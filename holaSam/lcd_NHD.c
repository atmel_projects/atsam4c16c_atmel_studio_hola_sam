/*
 * lcd_NHD.c
 *
 * Created: 22/01/2018 07:05:12 p.m.
 *  Author: hydro
 */

#include "lcd_NHD.h"
#include "imagenes.h"

void data_out(unsigned char i)
{
	unsigned int n, bit,t;
	unsigned char i_bit[8];
	
	bit = 1;
	for (n =0; n<=7; n++)
	{
		i_bit[n] = i && bit;	
		bit = bit << 1;
	}
	
	uint8_t tiempo_de_Delay = 1;
	LCD_CS_Low();
	LCD_A0_High();
	
		//Fila 7
		//LCD_MOSI_Low();
		LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();
		
		//Fila 6
		LCD_MOSI_Low();
		//LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();
		
		//Fila 5
		LCD_MOSI_Low();
		//LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();
		
		//Fila 4
		//LCD_MOSI_Low();
		LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();
		
		//Fila 3
		LCD_MOSI_Low();
		//LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();
		
		//Fila 2
		LCD_MOSI_Low();
		//LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();
		
		//Fila 1
		LCD_MOSI_Low();
		//LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();
		
		//Fila 0
		//LCD_MOSI_Low();
		LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();
		
	LCD_CS_High();	
}

void comm_out(unsigned char i)
{
	unsigned int n;
	uint8_t tiempo_de_Delay = 200;
	LCD_CS_Low();
	LCD_A0_Low();
	for(n=0;n<8;n++)
	{
		LCD_SCK_Low();
		//Delay_N_ms(tiempo_de_Delay);
		if(i & 0x80)
		{
			LCD_MOSI_High();
		}
		else{
			LCD_MOSI_Low();
		}
		i = i << 1;//Aqu� es donde se recorre el dato
		//Delay_N_ms(tiempo_de_Delay);
		Delay_N_us(tiempo_de_Delay);
		LCD_SCK_High();
		//Delay_N_ms(tiempo_de_Delay);
	}
	LCD_CS_High();	
}

void LCD_PIO_init()
{
	//HAbilitar relojes de perifericos PIO A y B
	REG_PMC_PCER0 = PMC_PCER0_PID11;
	REG_PMC_PCER0 = PMC_PCER0_PID12;
	//REG_SPI0_I
	
	LCD_CS_Out();
	LCD_CS_Pullup_En();
	LCD_CS_High();
	
	//LCD_RST_PIO_Dis();
	LCD_RST_Out();
	LCD_RST_Pullup_En();
	//LCD_RST_PIO_En();
	//LCD_RST_Low();
	LCD_RST_High();
	
	LCD_SCK_Out();
	LCD_SCK_PUllup_En();
	LCD_SCK_Low();
	//LCD_SCK_High();
	
	LCD_A0_Out();
	LCD_A0_Pullup_En();
	LCD_A0_Low();
	//LCD_A0_High();
	
	//REG_PIOA_PPDER |= PIO_PPDDR_P7;
	//LCD_MOSI_PIO_Dis();
	LCD_MOSI_Out();	
	//LCD_MOSI_Pullup_En();
	LCD_MOSI_PIO_En();
	//Delay_N_ms(100);
	LCD_MOSI_Low();
	//Delay_N_ms(100);
	//LCD_MOSI_High();
}

void encender_display(unsigned char i)
{
	comm_out(0xAE + i);
}


void ajustar_contraste(unsigned char i)
{
	comm_out(0x30 + i);
}

void blackout(unsigned char i)
{
	comm_out(0xA4 + i);
}

void rst_lcd()
{
	comm_out(0xE2);
}

void fn_pb(unsigned char i)
{
	comm_out(0xA6 + i);
}

void inv_scan(unsigned char i)
{
	if (i==1)
	{
		comm_out(0xC8);
	}else{
		comm_out(0xC0);
	}
}

void init_LCD_NHD()
{
	LCD_PIO_init();		//Inicializar pines
	rst_lcd();			//reseteo
	comm_out(0xA0);		//ADC Select
	//encender_display(0);//comm_out(0xAE);	//Apagar Display
	inv_scan(0);		//(0xC8);	//Normal Ouput Scan Direction//comm_out(0xC7)//comm_out(0xC8);
	fn_pb(0);			//comm_out(0xA7);	//Display NOrmal/reverse
	comm_out(0xA2);		//LCD Bias Set: 1/9 Bias
	comm_out(0x2F);		//Power Control Set:
	comm_out(0x21);		//Voltage regulator, internal resistor ratio
	comm_out(0x81);		//Electronic Module Set No se puede acceder a este comnado
	ajustar_contraste(0);//comm_out(0x30);	//Electronic volume register set (contraste)
	encender_display(1);
	blackout(1);
	blackout(0x00);
}


//void mosImg(unsigned char *cadena_lcd)
//{
	//unsigned int i,j;
	//unsigned char page = 0xB0;
	//comm_out(0xAE); //Apagar display
	//comm_out(0x40); //Direccion de inicio en ram
	//for(i=0;i<4;i++)
	//{
		//comm_out(page); //selecci�n de p�gina de escritura
		//comm_out(0x10); //Direccion de los 4 bits superiores de la columna
		//comm_out(0x00);	//Direccion de los 4 bits inferriores de la columna
		//for(j=0;j<128;j++)
		//{
			//data_out(*cadena_lcd);
			//cadena_lcd++;
		//}
		//page++;
	//}
	//comm_out(0xAF); //Enciende el display
//}

void mosImgRenglon(unsigned char *cadena_lcd, unsigned char page, unsigned char startPos)
{
	unsigned int i,j,sP_Upper,sP_Lower;
	sP_Upper = (startPos & 0xF0)>>4;
	sP_Lower = (startPos & 0x0F);
	//unsigned char page = 0xB0;
	page = 0xB0 + page;
	comm_out(0x40); //Direccion de inicio en ram
	comm_out(page); //selecci�n de p�gina de escritura
	comm_out(0x10+sP_Upper); //Direccion de los 4 bits superiores de la columna
	comm_out(0x00 + sP_Lower);	//Direccion de los 4 bits inferriores de la columna
	for(j=0;j<128;j++)
	{
		//data_out(*cadena_lcd);
		testColumna(*cadena_lcd);
		cadena_lcd++;
	}
	page++;
	//comm_out(0xAF); //Enciende el display
}

void mosLetra(unsigned char *cadena_lcd, unsigned char page, unsigned char startPos, unsigned char posArray, unsigned char cols)
{
	unsigned int i,j,sP_Upper,sP_Lower;
	sP_Upper = (startPos & 0xF0)>>4;
	sP_Lower = (startPos & 0x0F);
	//unsigned char page = 0xB0;
	page = 0xB0 + page;
	comm_out(0x40); //Direccion de inicio en ram
	comm_out(page); //selecci�n de p�gina de escritura
	comm_out(0x10+sP_Upper); //Direccion de los 4 bits superiores de la columna
	comm_out(0x00 + sP_Lower);	//Direccion de los 4 bits inferriores de la columna
	cadena_lcd = cadena_lcd + posArray;
	for(j=0;j<cols;j++)
	{
		//data_out(*cadena_lcd);
		testColumna(*cadena_lcd);
		cadena_lcd++;
	}
	page++;
	//comm_out(0xAF); //Enciende el display
}

void lcd_clear(unsigned int l)
{
	unsigned int i,j;
	unsigned int page = 0xB0;
	comm_out(0x40); //Direccion de inicio en ram
	
	for(i=0;i<4;i++)
	{
		comm_out(page); //selecci�n de p�gina de escritura
		comm_out(0x10);
		comm_out(0x00);
		for(j=0;j<128;j++)
		{
			if(l == 1)
			{
				null_data_per_column();	
			} else if (l == 0) {
				full_data_per_column();
			}
			
		}
		page++;
	}
	
	//comm_out(0xAF); //Enciende el display
}

void null_data_per_column()
{	
	LCD_CS_Low();
	LCD_A0_High();
	for(int n = 0;n<8;n++)
	{
		//Fila 7
		LCD_MOSI_Low();
		//LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();
	}	

	LCD_CS_High();	
}

void full_data_per_column()
{
	LCD_CS_Low();
	LCD_A0_High();
	
	for (int n =0; n<8;n++)
	{
		//Fila 7
		//LCD_MOSI_Low();
		LCD_MOSI_High();
		LCD_SCK_Low();
		LCD_SCK_High();	
	}
	
	LCD_CS_High();
}

void testColumna(unsigned char i)
{
	unsigned int n, bit,t;
	unsigned char i_bit[10];
	//i = 0xAF;
	bit = 1;
	for (n =0; n<8; n++)
	{
		i_bit[n] = i & bit;
		bit = bit << 1;
		i_bit[n] = i_bit[n] >> n;
	}
	int8_t v = 7;
	uint8_t tiempo_de_Delay = 1;
	LCD_CS_Low();
	LCD_A0_High();
	
	for (v = 7; v >= 0; v--)
	{
		if (i_bit[v] == 0)
		{
			LCD_MOSI_Low();
		} else if(i_bit[v] == 1)
		{
			LCD_MOSI_High();
		}
		Delay_N_us(tiempo_de_Delay);
		LCD_SCK_Low();
		LCD_SCK_High();
	}
	
	////Fila 7
	////LCD_MOSI_Low();
	////LCD_MOSI_Low();
	//if (i_bit[7] == 0)
	//{
		//LCD_MOSI_Low();	
	//} else if(i_bit[7] == 1)
	//{
		//LCD_MOSI_High();	
	//}
	//Delay_N_us(tiempo_de_Delay);
	//LCD_SCK_Low();
	//LCD_SCK_High();
	//
	////Fila 6
	////LCD_MOSI_Low();
	////LCD_MOSI_High();
	//if (i_bit[6] == 0)
	//{
		//LCD_MOSI_Low();
	//} else if(i_bit[6] == 1)
	//{
		//LCD_MOSI_High();
	//}
	//Delay_N_us(tiempo_de_Delay);
	//LCD_SCK_Low();
	//LCD_SCK_High();
	//
	////Fila 5
	////LCD_MOSI_Low();
	////LCD_MOSI_High();
	//if (i_bit[5] == 0)
	//{
		//LCD_MOSI_Low();
	//} else if(i_bit[5] == 1)
	//{
		//LCD_MOSI_High();
	//}
	//Delay_N_us(tiempo_de_Delay);
	//LCD_SCK_Low();
	//LCD_SCK_High();
	//
	////Fila 4
	////LCD_MOSI_Low();
	////LCD_MOSI_High();
	//if (i_bit[4] == 0)
	//{
		//LCD_MOSI_Low();
	//} else if(i_bit[4] == 1)
	//{
		//LCD_MOSI_High();
	//}
	//LCD_SCK_Low();
	//LCD_SCK_High();
	//
	////Fila 3
	////LCD_MOSI_Low();
	////LCD_MOSI_High();
	//if (i_bit[3] == 0)
	//{
		//LCD_MOSI_Low();
	//} else if(i_bit[3] == 1)
	//{
		//LCD_MOSI_High();
	//}
	//LCD_SCK_Low();
	//LCD_SCK_High();
	//
	////Fila 2
	////LCD_MOSI_Low();
	////LCD_MOSI_High();
	//if (i_bit[2] == 0)
	//{
		//LCD_MOSI_Low();
	//} else if(i_bit[2] == 1)
	//{
		//LCD_MOSI_High();
	//}
	//LCD_SCK_Low();
	//LCD_SCK_High();
	//
	////Fila 1
	////LCD_MOSI_Low();
	////LCD_MOSI_High();
	//if (i_bit[1] == 0)
	//{
		//LCD_MOSI_Low();
	//} else if(i_bit[1] == 1)
	//{
		//LCD_MOSI_High();
	//}
	//LCD_SCK_Low();
	//LCD_SCK_High();
	//
	////Fila 0
	////LCD_MOSI_Low();
	////LCD_MOSI_High();
	//if (i_bit[0] == 0)
	//{
		//LCD_MOSI_Low();
	//} else if(i_bit[0] == 1)
	//{
		//LCD_MOSI_High();
	//}
	//LCD_SCK_Low();
	//LCD_SCK_High();
	
	LCD_CS_High();
}

void mosColumna(unsigned char *cadena_lcd, unsigned char page, unsigned char startPos)
{
	unsigned int i,j,sP_Upper,sP_Lower;
	sP_Upper = (startPos & 0xF0)>>4;
	sP_Lower = (startPos & 0x0F);
	//unsigned char page = 0xB0;
	page = 0xB0 + page;
	comm_out(0x40); //Direccion de inicio en ram
	comm_out(page); //selecci�n de p�gina de escritura
	comm_out(0x10+sP_Upper); //Direccion de los 4 bits superiores de la columna
	comm_out(0x00 + sP_Lower);	//Direccion de los 4 bits inferriores de la columna
	testColumna(*cadena_lcd);
	//comm_out(0xAF); //Enciende el display
}

void mosCidesi()
{
	//char V = 21;
	//char I = 8;
	//char W = 22;
	//char H = 7;
	mosImgRenglon(cidesi,3,0);
	mosImgRenglon(hv_page2,2,0);
	mosImgRenglon(hv_page1,1,0);
	mosLetra(letras,3,35,le_pos[lV],le_col[lV]);mosLetra(numeros,3,35+le_col[lV],num_pos[ddot],num_col[ddot]);//V:
	mosLetra(letras,2,35,le_pos[lI],le_col[lI]);mosLetra(numeros,2,35+le_col[lI],num_pos[ddot],num_col[ddot]);//I:
	mosLetra(letras,1,35,le_pos[lW],le_col[lW]);mosLetra(numeros,1,35+le_col[lW],num_pos[ddot],num_col[ddot]);//W:
	mosLetra(letras,0,35,le_pos[lW],le_col[lW]);mosLetra(letras,0,35+le_col[lW],le_pos[lH],le_col[lH]);mosLetra(numeros,60,35+le_col[lW]+le_col[lH],num_pos[ddot],num_col[ddot]);//Wh:
	mosLetra(octa_sig,0,122,0,5);
}

void mosVol(unsigned char *array)
{	
	char V_c= array[4];
	char V_d = array[3];
	char V_u = array[2];
	char V_dec1 = array[1];
	char V_dec2 = array[0];
	char pos=43; //posici�n de escritura en la p�gina
	mosLetra(numeros,3,pos,num_pos[V_c],num_col[V_c]);//centena
	mosLetra(numeros,3,pos+num_col[V_c],num_pos[V_d],num_col[V_d]);//decena
	mosLetra(numeros,3,pos+num_col[V_c]+num_col[V_d],num_pos[V_u],num_col[V_u]);//unidad
	mosLetra(numeros,3,pos+num_col[V_c]+num_col[V_d]+num_col[V_u],num_pos[dot], num_col[dot]);//punto
	mosLetra(numeros,3,pos+num_col[V_c]+num_col[V_d]+num_col[V_u]+num_col[dot], num_pos[V_dec1], num_col[V_dec1]);//decimal 1
	mosLetra(numeros,3,pos+num_col[V_c]+num_col[V_d]+num_col[V_u]+num_col[dot]+num_col[V_dec1], num_pos[V_dec2], num_col[V_dec2]);// decimal 2
}

void mosCur(unsigned char *array)
{
		char V_u = array[3];
		char V_dec1 = array[2];
		char V_dec2 = array[1];
		char V_dec3 = array[0];
		char pos=43; //posici�n de escritura en la p�gina
		mosLetra(numeros, 2, pos, num_pos[V_u], num_col[V_u]);//unidad
		mosLetra(numeros, 2, pos+num_col[V_u], num_pos[dot], num_col[dot]);//punto
		mosLetra(numeros, 2, pos+num_col[V_u]+num_col[dot], num_pos[V_dec1], num_col[V_dec1]);//dec1
		mosLetra(numeros, 2, pos+num_col[V_u]+num_col[dot]+num_col[V_dec1], num_pos[V_dec2], num_col[V_dec2]);//dec2
		mosLetra(numeros, 2, pos+num_col[V_u]+num_col[dot]+num_col[V_dec1]+num_col[V_dec2], num_pos[V_dec3], num_col[V_dec3]);//dec3
}

void mosWat(unsigned char *array)
{
	char V_u = array[1];
	char V_dec1 = array[0];
	char pos=43+1; //posici�n de escritura en la p�gina
	mosLetra(numeros, 1, pos,num_pos[V_u],num_col[V_u]);//unidad
	mosLetra(numeros, 1, pos+num_col[V_u], num_pos[dot], num_col[dot]);//punto
	mosLetra(numeros, 1, pos+num_col[V_u]+num_col[dot], num_pos[V_dec1], num_col[V_dec1]);//centena 
	
	//mosLetra(numeros,1,pos,num_pos[V_c],num_col[V_c]);//centena
	//mosLetra(numeros,1,pos+num_col[V_c],num_pos[V_d],num_col[V_d]);//decena
	//mosLetra(numeros,1,pos+num_col[V_c]+num_col[V_d],num_pos[V_u],num_col[V_u]);//unidad
	//mosLetra(numeros,1,pos+num_col[V_c]+num_col[V_d]+num_col[V_u],num_pos[dot], num_col[dot]);//punto
	//mosLetra(numeros,1,pos+num_col[V_c]+num_col[V_d]+num_col[V_u]+num_col[dot], num_pos[V_dec1], num_col[V_dec1]);//decimal 1
	//mosLetra(numeros,1,pos+num_col[V_c]+num_col[V_d]+num_col[V_u]+num_col[dot]+num_col[V_dec1], num_pos[V_dec2], num_col[V_dec2]);// decimal 2
}
void mosCon(unsigned char *array)
{
	//char V_c= 1;
	//char V_d = 5;
	char V_u = array[2];
	char V_dec1 = array[1];
	char V_dec2 = array[0];
	char pos=43+1+4; //posici�n de escritura en la p�gina
	//mosLetra(numeros,0,pos,num_pos[V_c],num_col[V_c]);//centena
	//mosLetra(numeros,0,pos+num_col[V_c],num_pos[V_d],num_col[V_d]);//decena
	mosLetra(numeros, 0, pos, num_pos[V_u], num_col[V_u]);//unidad
	mosLetra(numeros, 0, pos+num_col[V_u], num_pos[dot], num_col[dot]);//punto
	mosLetra(numeros, 0, pos+num_col[V_u]+num_col[dot], num_pos[V_dec1], num_col[V_dec1]);//decimal 1
	mosLetra(numeros, 0, pos+num_col[V_u]+num_col[dot]+num_col[V_dec1], num_pos[V_dec2], num_col[V_dec2]);// decimal 2
}

void renglon_clear(unsigned char renglon, unsigned char pos)
{
	mosImgRenglon(renglon_vacio_test,renglon, pos);
}